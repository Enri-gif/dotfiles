{ config, lib, ... }:
let
  package = "Package name";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption {
        default = true;
        };
    };

    config = lib.mkIf config.${package}.enable {
        programs.${package} = {
            enable = true;
        };
    };
}

