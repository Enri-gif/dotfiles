self: super:
{
    dtkit-patch = super.rustPlatform.buildRustPackage rec {
        pname = "dtkit-patch";
        version = "0.1.6";
        
        src = super.fetchFromGitHub {
            owner = "manshanko";
            repo = pname;
            rev = version;
            hash = "sha256-pGTS0Jk6ZxJj36cjQty/fLKDi67SVPBOp/wyylIfWZ0=";
        };

        cargoLock = {
            lockFile = ./Cargo.lock;
            outputHashes = {
                 "steam_find-0.1.2" = "sha256-lH03qTPNvKbGNB2+RfI3BTgbOJ+T/R7q2kGcv8zoflg=";
            };
        };

        meta = {
            description = "dtkit-patch for darktide modloader";
            homepage = "https://github.com/manshanko/dtkit-patch";
            license = "";
            maintainers = [];
        };

        # buildPhase = ''
        #     cargo build --release
        #     # ls target/release
        # '';
        #
        # installPhase = ''
        #     mkdir -p $out/opt/dtkit-patch
        #     # echo "HERE!!"
        #     # pwd
        #     cp target/release/dtkit-patch $out/opt/dtkit-patch/
        #     # ls target/release
        #     # cp target/release/dtkit-patch $out/opt/dtkit-patch/
        #     cp $out/opt/dtkit-patch/dtkit-patch /home/erik/.var/app/com.valvesoftware.Steam/.local/share/Steam/steamapps/common/Warhammer\ 40\,000\ DARKTIDE/tools
        # '';
        
        # passthru = {
        #     # A post-installation command, for example, linking the file to a location
        #     extraInstallCommands = ''
        #         ln -s $out/bin/dtkit-patch /home/erik/.var/app/com.valvesoftware.Steam/.local/share/Steam/steamapps/common/Warhammer\ 40,000\ DARKTIDE/tools/dtkit-patch
        #     '';
        # };
    };
}
