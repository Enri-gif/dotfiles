{ config, pkgs, lib, ... }:
{
    options = {
        neovim.enable = lib.mkEnableOption "enables mpv";
    };

    config = lib.mkIf config.mpv.enable {
        programs.neovim = {
            enable = true;
            defaultEditor = true;
        };
    };
}
