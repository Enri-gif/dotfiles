{ config, lib, pkgs, ... }:
{
    options = {
        hyprland.enable = lib.mkEnableOption "enables hyprland";
    };

    config = lib.mkIf config.hyprland.enable {
        wayland.windowManager.hyprland = {
            enable = true;
            settings = {

                # variables
                "$terminal" = "kitty";
                "$fileManager" = "lf";
                "$menu" = "tofi-run | xargs hyprctl dispatch exec --";
                "$mainMod" = "SUPER";
                "$switch_window" = "window_switcher";

                # environment variables
                env = [
                    "XCURSOR_SIZE, 24"
                    "HYPRCURSOR_SIZE, 24"
                ];

                monitor = [
                    "DP-2, 1920x1080@144, 0x0 , 1"
                    "DP-1, 1920x1080@60, -1920x0, 1"
                    "HDMI-A-1, 1920x1080@60, 1920x0 , 1"
                    "Unkown-1, disable"
                ];

                workspace = [
                    "1, monitor:DP-2"
                    "3, monitor:DP-1"
                    "4, monitor:HDMI-A-1"
                ];

                # Ecosystem = {
                #     no_update_news = true;
                #     no_donation_nag = true;
                # };

                general = {
                    gaps_in = 5;
                    gaps_out = 20;
                    border_size = 2;
                    no_focus_fallback = true;

                    # Set to true enable resizing windows by clicking and dragging on borders and gaps
                    resize_on_border = false ;

                    allow_tearing = false;
                    layout = "master";
                };

                cursor = {
                    inactive_timeout = 60;
                    default_monitor = "DP-1";
                };

                decoration = {
                    rounding = 10;
                    active_opacity = 1.0;
                    inactive_opacity = 0.9;

                    blur = {
                        enabled = true;
                        size = 3;
                        vibrancy = 0.1696;
                    };
                };

                animations = {
                    enabled = true;
                    bezier = "myBezier, 0.05, 0.9, 0.1, 1.05";
                    animation = [
                        "windows, 1, 7, myBezier"
                        "windowsOut, 1, 7, default, popin 80%"
                        "border, 1, 10, default"
                        "borderangle, 1, 8, default"
                        "fade, 1, 7, default"
                        "workspaces, 1, 6, default"
                    ];
                };

                master = {
                    new_status = "master";
                    new_on_top = true;
                };

                misc = {
                    force_default_wallpaper = 0;
                    disable_hyprland_logo = true;
                    initial_workspace_tracking = 2;
                };

                input = {
                    kb_layout = "dk";
                    follow_mouse = 1;
                    sensitivity = 0; # -1.0 - 1.0, 0 means no modification.
                };

                device = {
                    name = "epic-mouse-v1";
                    sensitivity = 0.5;
                };

                bindm = [
                    # Move/resize windows with mainMod + LMB/RMB and dragging
                    "$mainMod, mouse:272, movewindow"
                    "$mainMod, mouse:273, resizewindow"
                ];
                
                # media keys
                ## l -> do stuff even when locked
                ## e -> repeats when key is held 
                # bindl = [
                #     "XF86AudioPlay, exec, /bin/sh -c 'playerctl  play-pause'" #--player=spotify play-pause 
                #     "XF86AudioNext, exec, /bin/sh -c 'playerctl next'" #--player=spotify next 
                #     "XF86AudioPrev, exec, /bin/sh -c 'playerctl previous'" #--player=spotify previous
                # ];
                #
                # bindle = [
                #     "XF86AudioRaiseVolume, exec, /bin/sh -c 'wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+'"
                #     "XF86AudioLowerVolume, exec, /bin/sh -c 'wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-'"
                # ];

                bind = [
                    "$mainMod, Return, exec, $terminal"
                    "$mainMod SHIFT, W, killactive"
                    "$mainMod, M, exit"
                    "$mainMod, E, exec, $fileManager"
                    "$mainMod, V, togglefloating"
                    "$mainMod, R, exec, $menu"
                    "$mainMod, TAB, exec, $switch_window"
                    "$mainMod, J, movefocus, l"
                    "$mainMod, K, movefocus, r"
                    "$mainMod, H, movefocus, u"
                    "$mainMod, L, movefocus, d"

                    # Switch workspaces with mainMod + [0-9]
                    "$mainMod, 1, workspace, 1"
                    "$mainMod, 2, workspace, 2"
                    "$mainMod, 3, workspace, 3"
                    "$mainMod, 4, workspace, 4"
                    "$mainMod, 5, workspace, 5"
                    "$mainMod, 6, workspace, 6"
                    "$mainMod, 7, workspace, 7"
                    "$mainMod, 8, workspace, 8"
                    "$mainMod, 9, workspace, 9"
                    "$mainMod, 0, workspace, 10"

                    # Move active window to a workspace with mainMod + SHIFT + [0-9]
                    "$mainMod SHIFT, 1, movetoworkspace, 1"
                    "$mainMod SHIFT, 2, movetoworkspace, 2"
                    "$mainMod SHIFT, 3, movetoworkspace, 3"
                    "$mainMod SHIFT, 4, movetoworkspace, 4"
                    "$mainMod SHIFT, 5, movetoworkspace, 5"
                    "$mainMod SHIFT, 6, movetoworkspace, 6"
                    "$mainMod SHIFT, 7, movetoworkspace, 7"
                    "$mainMod SHIFT, 8, movetoworkspace, 8"
                    "$mainMod SHIFT, 9, movetoworkspace, 9"
                    "$mainMod SHIFT, 0, movetoworkspace, 10"

                    # Move workspaces
                    "$mainMod SHIFT, comma, movecurrentworkspacetomonitor, l"
                    "$mainMod SHIFT, period, movecurrentworkspacetomonitor, r" 
                    #$mainMod SHIFT, period, swapactiveworkspaces, e r 
                    #$mainMod SHIFT, comma, swapactiveworkspaces, e l

                    #"$mainMod, comma, focusmonitor, l"
                    #"$mainMod, period, focusmonitor, r"

                    "$mainMod, ESCAPE, exec, wlogout"


                    # Example special workspace (scratchpad)
                    "$mainMod, S, togglespecialworkspace, magic"
                    "$mainMod SHIFT, S, movetoworkspace, special:magic"

                    # Scroll through existing workspaces with mainMod + scroll
                    "$mainMod, mouse_down, workspace, e+1"
                    "$mainMod, mouse_up, workspace, e-1"


                    # fullscreen
                    "$mainMod, F, fullscreen"

                    # stacking layout
                    "$mainMod SHIFT, e, exec, togglegroup"

                    # Move window with mainMod + motion keys
                    "$mainMod SHIFT, h, movewindoworgroup, l"
                    "$mainMod SHIFT, l, movewindoworgroup, r"
                    "$mainMod SHIFT, k, movewindoworgroup, u"
                    "$mainMod SHIFT, j, movewindoworgroup, d"

                    # hyprshot bindings
                    ## Screenshot a monitor
                    "$mainMod SHIFT, p, exec, hyprshot -m output --clipboard-only"
                    ## Screenshot a region
                    "$mainMod, p, exec, hyprshot -m region --clipboard-only"
                ];

                windowrulev2 = [
                    "opacity 0.95 0.8, class:^(kitty)$"
                    "opacity 1 2., class:^(firefox)$"
                    "opacity 1 2., class:^(org.qutebrowser.qutebrowser)$"
                    "suppressevent maximize, class:.*"
                ];

                exec-once = [
                    "autostart&"
                    "[workspace 1 silent] qutebrowser"
                    "[workspace 3 silent] dev.vencord.Vesktop"
                    "[workspace 3 silent] com.spotify.Client"
                ];
            };
        };
    };
}
