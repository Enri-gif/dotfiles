{ config, lib, ... }:
let
    package = "fzf";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };

    config = lib.mkIf config.${package}.enable {
        programs.${package} = {
            enable = true;
        };
    };
}
