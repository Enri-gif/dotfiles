{ config, lib, ... }:
let
    package = "btop";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };

    config = lib.mkIf config.${package}.enable {
        programs.${package} = {
            enable = true;
            settings = lib.mkDefault {
                color_theme = "Default";
                theme_background = false;
            };
        };
    };
}
