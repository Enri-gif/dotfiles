{ config, lib, ... }:
let
  package = "firefox";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption {
        default = true;
        };
    };

    config = lib.mkIf config.${package}.enable {
        programs.${package} = {
            enable = true;
            policies = {
                Cookies.Behavior = "reject-tracker-and-partition-foreign";
                DisablePocket = true;
                DisableTelemerty = true;
                DNSOverHTTPS.Enabled = true;
                SearchEngines.Default = "DuckDuckGo";
            };
        };
    };
}

