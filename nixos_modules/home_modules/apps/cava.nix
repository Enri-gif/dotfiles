{ config, lib, ... }:
let
    package = "cava";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };

    config = lib.mkIf config.${package}.enable {
        programs.${package} = {
            enable = true;
            settings = {
                color = {
                    background = "'#000000'";
                    foreground = "'#B026FF'";
                    gradient = 1;
                    gradient_count = 8;
                    gradient_color_1 = "'#4443DC'";
                    gradient_color_2 = "'#6A43DC'";
                    gradient_color_3 = "'#8043DC'";
                    gradient_color_4 = "'#9E43DC'";
                    gradient_color_5 = "'#CB43DC'";
                    gradient_color_6 = "'#DC43BA'";
                    gradient_color_7 = "'#DC4394'";
                    gradient_color_8 = "'#DC4369'";
                };
            };
        };
    };
}
