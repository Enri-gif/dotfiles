{ config, lib, ... }:
let
  package = "fastfetch";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption {
        default = true;
            description = "enables fastfetch";
        };
    };

    config = lib.mkIf config.${package}.enable {
        programs.${package} = {
            enable = true;
        };
    };
}

