{ config, pkgs, lib, ... }:
{
    options = {
        pywal.enable = lib.mkEnableOption "enables pywal";
    };

    config = lib.mkIf config.pywal.enable {
        programs.pywal = {
            enable = true;
        };
    };
}
