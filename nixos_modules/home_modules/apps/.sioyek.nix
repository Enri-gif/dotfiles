{ config, lib, ... }: 
let
    package = "sioyek";
in
{
  options = {
    ${package}.enable = lib.mkEnableOption {
      default = true;
      description = "enables ${package}";
    };
  };
  
  config = lib.mkIf config.${package}.enable {
    programs.${package} = {
	enable = true;
    };
  };
}

