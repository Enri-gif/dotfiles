{ config, lib, ... }:
let
    package = "qutebrowser";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };

    config = lib.mkIf config.${package}.enable {
        programs.${package} = {
            enable = true;

            settings = {
                url.start_pages = [ "youtube.com" ];

                auto_save.session = true;

                colors.webpage.darkmode.enabled = true;

                content = {
                    autoplay = false;
                    site_specific_quirks.enabled = true;
                    webgl = true;

                    cookies = {
                        store = false;
                        accept = "no-3rdparty";
                    };

                    blocking = {
                        enabled = true;
                        method = "both";
                        adblock.lists = [
                            # "https://easylist.to/easylist/easylist.txt"
                            "https://easylist.to/easylist/easyprivacy.txt"
                            "https://secure.fanboy.co.nz/fanboy-annoyance.txt"
                            "https://easylist-downloads.adblockplus.org/antiadblockfilters.txt"
                            "https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt"
                        ];
                        hosts.lists = [
                            "https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts"
                            "https://www.malwaredomainlist.com/hostslist/hosts.txt"
                            "http://someonewhocares.org/hosts/hosts"
                            "http://winhelp2002.mvps.org/hosts.zip"
                            "http://malwaredomains.lehigh.edu/files/justdomains.zip"
                            "https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&mimetype=plaintext"
                        ];
                    };

                    headers = {
                        user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/132.0.0.0 Safari/537.3";
                        accept_language = "en-US,en;q=0.5";
                        referer = "same-domain";
                        do_not_track = true;
                    };
                };

                downloads ={
                    prevent_mixed_content = true;
                    remove_finished = 500;
                };

                editor.command = ["kitty" "-e" "nvim" "{file}" "-c" "normal {line}G{column0}l"];

            };
            searchEngines = {
                DEFAULT =  "search.brave.com/search?q={}";
    	        yt =       "https://www.youtube.com/results?search_query={}";
                nx =       "mynixos.com/search?q={}";
                nw =       "https://wiki.nixos.org/index.php?search={}";
                gh =       "https://github.com/search?q={}";
                mal =      "https://myanimelist.net/search/all?q={}&cat=all";
                pdb =      "https://www.protondb.com/search?q={}";
                ani =      "https://aniwatchtv.to/search?keyword={}";
            };
        };
    };
}
