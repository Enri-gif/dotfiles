{ config, lib, ... }:
let
  package = "wofi";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption {
        default = true;
        };
    };

    config = lib.mkIf config.${package}.enable {
        programs.${package} = {
            enable = true;
        };
    };
}

