{ config, lib, ... }:
let
    package = "kitty";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };

    config = lib.mkIf config.${package}.enable {
        programs.${package} = {
            enable = true;
            shellIntegration.enableZshIntegration = true;
            extraConfig = ''
                map ctrl+shift+enter new_window_with_cwd
            '';
        };
    };
}
