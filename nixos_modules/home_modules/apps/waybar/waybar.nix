{ config, lib, ... }:
{

    options = {
        waybar.enable = lib.mkEnableOption "enables waybar";
    };

    config = lib.mkIf config.waybar.enable {
        programs.waybar = {
            enable = true;
            systemd.enable = true;
            style = lib.mkForce ./styles/round_square_translucent/style.css;
            settings = {
                bar1 = {
                    height = 45;
                    layer = "top";
                    position = "top";
                    #include = ./default-modules.json;
                    output = [ "DP-2" ];
                    modules-left = [ "hyprland/workspaces" "mpris" ];
                    modules-center = [ "hyprland/window" ];
                    modules-right = [ "cpu" "memory" "pulseaudio" "tray" "clock" ];

                    "hyprland/workspaces" = {
                        #format = "<sub>{icon}</sub>\n{name}";
                        active-only = false;
                    };

                    "hyprland/window" = {
                        separate-outputs = true;
                        icon-size = 16;
                        icon = true;
                        max-length = 40;
                    };

                    "mpris" = {
                        format = "{player_icon} {status_icon} {dynamic}";
                        format-playing = "{player_icon} {status_icon} {dynamic}";
                        format-paused = "{player_icon} {status_icon} {dynamic}";
                        format-stopped = "{status_icon} {dynamic}";
                        player-icons = {
                            default = "🎵";
                            spotify = "";
                        };
                        status-icons = {
                            paused = " ⏸ ";
                            playing = " 󰐊 ";
                            stopped = " 󰓛 ";
                        };
                        ignored-players = [ "firefox" "chromium" ];
                        dynamic-order = [ "title" "artist" "album" ];
                        dynamic-importance-order = [ "title" "artist" "album" ];
                        dynamic-len = 50;
                    };

                    "tray" = {
                        spacing = 10;
                    };

                    "clock" = {
                        tooltip-format = "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
                        #format = "{:L%Y-%m-%d<small>[%a]</small> <tt><small>%p</small></tt>%H:%M}";
                        format = "{:%a %b %d %R}";
                    };

                    "cpu" = {
                        format = " {usage}%";
                    };

                    "memory" = {
                        format = " {used:0.1f}G/{total:0.1f}G";
                    };

                    "pulseaudio" = {
                        scroll-step = 5;
                        format = " {volume}% {format_source}";
                        format-muted = "󰝟 {format_source}";
                        format-source = " {volume}%";
                        format-source-muted = " {volume}%";
                        on-click = "pavucontrol";
                        on-click-right = "foot -a pw-top pw-top";
                    };
                };

                bar2 = {
                    height = 45;
                    layer = "top";
                    position = "top";
                    #include = ./default-modules.json;
                    output = [ "HDMI-A-1" ];
                    modules-left = [ "hyprland/workspaces" "mpris" ];
                    modules-center = [ "hyprland/window" ];
                    modules-right = [ "cpu" "memory" "pulseaudio" "tray" "clock" ];

                    "hyprland/window" = {
                        separate-outputs = true;
                        icon-size = 16;
                        icon = true;
                        max-length = 40;
                    };

                    "mpris" = {
                        format = "{player_icon} {dynamic}";
                        format-playing = "{player_icon} {status_icon} {dynamic}";
                        format-paused = "{player_icon} {status_icon} {dynamic}";
                        format-stopped = "{status_icon} {dynamic}";
                        player-icons = {
                            default = "🎵";
                            spotify = "";
                        };
                        status-icons = {
                            paused = "⏸";
                            playing = "󰐊";
                            stopped = "󰓛";
                        };
                        ignored-players = [ "firefox" "chromium" ];
                        dynamic-order = [ "title" "artist" "album" ];
                        dynamic-importance-order = [ "title" "artist" "album" ];
                        dynamic-len = 30;
                    };

                    "tray" = {
                        spacing = 10;
                    };

                    "clock" = {
                        tooltip-format = "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
                        format = "{:%a %b %d %R}";
                    };

                    "cpu" = {
                        format = " {usage}%";
                    };

                    "memory" = {
                        format = " {used:0.1f}G/{total:0.1f}G";
                    };

                    "pulseaudio" = {
                        scroll-step = 5;
                        format = " {volume}% {format_source}";
                        format-muted = "󰝟 {format_source}";
                        format-source = " {volume}%";
                        format-source-muted = " {volume}%";
                        on-click = "pavucontrol";
                    };
                };

                bar3 = {
                    height = 45;
                    layer = "top";
                    position = "top";
                    #include = ./default-modules.json;
                    output = [ "DP-1" ];
                    modules-left = [ "hyprland/workspaces" "mpris" ];
                    modules-center = [ "hyprland/window" ];
                    modules-right = [ "cpu" "memory" "pulseaudio" "tray" "clock" ];

                    "hyprland/window" = {
                        separate-outputs = true;
                        icon-size = 16;
                        icon = true;
                        max-length = 40;
                    };

                    "mpris" = {
                        format = "{player_icon} {dynamic}";
                        format-playing = "{player_icon} {status_icon} {dynamic}";
                        format-paused = "{player_icon} {status_icon} {dynamic}";
                        format-stopped = "{status_icon} {dynamic}";
                        player-icons = {
                            default = "🎵";
                            spotify = "";
                        };
                        status-icons = {
                            paused = "⏸";
                            playing = "󰐊";
                            stopped = "󰓛";
                        };
                        ignored-players = [ "firefox" "chromium" ];
                        dynamic-order = [ "title" "artist" "album" ];
                        dynamic-importance-order = [ "title" "artist" "album" ];
                        dynamic-len = 30;
                    };

                    "tray" = {
                        spacing = 10;
                    };

                    "clock" = {
                        tooltip-format = "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
                        format = "{:%a %b %d %R}";
                    };

                    "cpu" = {
                        format = " {usage}%";
                    };

                    "memory" = {
                        format = " {used:0.1f}G/{total:0.1f}G";
                    };

                    "pulseaudio" = {
                        scroll-step = 5;
                        format = " {volume}% {format_source}";
                        format-muted = "󰝟 {format_source}";
                        format-source = " {volume}%";
                        format-source-muted = " {volume}%";
                        on-click = "pavucontrol";
                    };
                };
            };
        };

    };

}
