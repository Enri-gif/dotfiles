{ config, lib, ... }:
let
    package = "lazygit";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };

    config = lib.mkIf config.${package}.enable {
        programs.${package} = {
            enable = true;
            settings = {

                gui.theme = lib.mkDefault {
                    lightTheme = false;
                    activeBorderColor = [ "blue" "bold" ];
                    inactiveBorderColor = [ "black" ];
                    selectedLineBgColor = [ "default" ];
                };
            };
        };
    };
}
