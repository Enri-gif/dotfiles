{ config, lib, ... }:
let
    package = "git";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };

    config = lib.mkIf config.${package}.enable {
        programs.${package} = {
            enable = true;
            userName = "Enri-gif";
            userEmail = "erikwesterberg60@gmail.com";
            extraConfig = {
                init.defaultBranch = "main";
                safe.directory = "/home/erik/.dotfiles";
                credential = {
                    helper = "keepassxc --git-groups";
                };
            };
        };
    };
}
