{ config, lib, ... }:
let
    package = "wlogout";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };

    config = lib.mkIf config.${package}.enable {
        programs.${package} = {
            enable = true;
            style = ./style.css;
            layout = [
                { 
                   label = "lock";
                   action = "hyprlock";
                   text = "Lock";
                   keybind = "l";
                }
                {
                   label = "hibernate";
                   action = "systemctl hibernate";
                   text = "Hibernate";
                   keybind = "h";
                }
                {
                   label = "logout";
                   action = "loginctl terminate-user $USER";
                   text = "Logout";
                   keybind = "e";
                }
                {
                   label = "shutdown";
                   action = "systemctl poweroff";
                   text = "Shutdown";
                   keybind = "s";
                }
                {
                   label = "suspend";
                   action = "systemctl suspend";
                   text = "Suspend";
                   keybind = "u";
                }
                {
                   label = "reboot";
                   action = "systemctl reboot";
                   text = "Reboot";
                   keybind = "r";
                }
            ];
        };
    };
}
