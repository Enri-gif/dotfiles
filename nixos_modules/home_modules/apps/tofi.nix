{ config, lib, ... }:
let
    package = "tofi";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };

    config = lib.mkIf config.${package}.enable {
        programs.${package} = {
            enable = true;
            settings = {
                width = "100%";
                height = "100%";
                border-width = 0;
                outline-width = 0;
                padding-left = "35%";
                padding-top = "35%";
                result-spacing = 25;
                num-results = 5;
                font = lib.mkDefault "monospace";
                background-color = lib.mkDefault "#000A";
            };
        };
    };
}
