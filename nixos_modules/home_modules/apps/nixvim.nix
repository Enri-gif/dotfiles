{ config, lib, inputs, ... }:
let
    package = "nixvim";
in
{
    imports = [ inputs.nixvim.homeManagerModules.nixvim ];

    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };
    config = lib.mkIf config.${package}.enable {
        programs.${package} = {
            enable = true;
            defaultEditor = true;
            enableMan = true;
            vimAlias = true;
            globals.mapleader = ",";
            opts = {
                number = true;         
                relativenumber = true; 
                shiftwidth = 4;
                expandtab = true;
                autoindent = true;
                smartindent = true;
                clipboard.register = "wl-clipboard";
            };
            keymaps = [
                {
                    action = "<cmd>bnext<cr>";
                    key = "<leader>n";
                }
                {
                    action = "<cmd>bprev<cr>";
                    key = "<leader>b";
                }
                {
                    action = "<cmd>CHADopen<cr>";
                    key = "<leader>t";
                }
                {
                    action = "<cmd>bd<cr>";
                    key = "<leader>q";
                }
                {
                    action = "<cmd>LazyGit<cr>";
                    key = "<leader>gg";
                }
                {
                    action = "<cmd>Telescope find_files<cr>";
                    key = "<leader>f";
                }
                {
                    action = "<cmd>Telescope buffers<cr>";
                    key = "<leader>fb";
                }
                {
                    action = "<cmd>nvim.lsp.buf.definition<cr>";
                    key = "<leader>ad";
                }

            ];

            plugins = {
                lsp = {
                    enable = true;
                    servers = {
                        lua_ls.enable = true;
                        jsonls.enable = true;
                        nil_ls.enable = true;
                        cssls.enable = true;
                        bashls.enable = true;
                        html.enable = true;
                        rust_analyzer = {
                            enable = true;
                            installCargo = true;
                            installRustc = true;
                        };
                    };
                };
                bufferline = {
                    enable = true;
                    settings.options = {
                        always_show_bufferline = true;
                    };
                };
                cmp = {
                    enable = true;
                    autoEnableSources = true;
                    settings = {
                        mapping = {
                            "<C-Space>" = "cmp.mapping.complete()";
                            "<C-d>" = "cmp.mapping.scroll_docs(-4)";
                            "<C-e>" = "cmp.mapping.close()";
                            "<C-f>" = "cmp.mapping.scroll_docs(4)";
                            "<CR>" = "cmp.mapping.confirm({ select = true })";
                            "<S-Tab>" = "cmp.mapping(cmp.mapping.select_prev_item(), {'i', 's'})";
                            "<Tab>" = "cmp.mapping(cmp.mapping.select_next_item(), {'i', 's'})";
                        };
                        sources = [
                            {
                                name = "nvim_lsp";
                            }
                            {
                                name = "luasnip";
                            }
                            {
                                name = "path";
                            }
                            {
                                name = "buffer";
                            }
                        ];
                    };
                };
                treesitter = {
                    enable = true;
                    settings.highlight.enable = true;
                };
                otter = {
                    enable = true;
                    autoActivate = true;
                };

                telescope = {
                    enable = true;
                    extensions.live-grep-args.enable = true;
                };
                
                lint.enable = true;
                web-devicons.enable = true;
                chadtree.enable = true;
                indent-blankline.enable = true;
                cmp-buffer.enable = true;
                cmp-calc.enable = true;
                cmp-cmdline.enable = true;
                cmp-cmdline-history.enable = true;
                cmp-nvim-lsp.enable = true;
                cmp-path.enable = true;
                cmp_luasnip.enable = true;
                gitgutter.enable = true;
                lazygit.enable = true;
                lualine.enable = true;
                which-key.enable = true;
                luasnip.enable = true;
                comment.enable = true;
            };

            extraConfigLua = ''
		local opts = { noremap=true, silent=true }
                local function quickfix()
                    vim.lsp.buf.code_action({
                        filter = function(a) return a.isPreferred end,
                        apply = true
                    })
                end

                vim.keymap.set('n', '<leader>x', quickfix, opts)
            '';
        };
    };
}
