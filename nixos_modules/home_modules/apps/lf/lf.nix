{ config, pkgs, lib, osConfig, ... }:
{

    options = {
        lf.enable = lib.mkEnableOption "enables lf";
    };

    config = lib.mkIf config.lf.enable {

        # icons
        xdg.configFile."lf/icons".source = ./icons;

        programs.lf = {
            enable = true;
            commands = {
                #Drag and drop files
                dragon-out = ''%${pkgs.xdragon}/bin/xdragon -a -x "$fx"'';
                editor-open = ''$$EDITOR $f'';
                mkdirfile = ''
                ''${{
                    printf "Directory Name: "
                    read DIR
                    mkdir $DIR
                }}
                '';
            };

            keybindings = {
                "\\\"" = "";
                #o = "";
                #d = "";
                #e = "";
                #f = "";
                #c = "mkdirfile";
                "." = "set hidden!";
                D = "delete";
                p = "paste";
                dd = "cut";
                y = "copy";
                "`" = "mark-load";
                "\\'" = "mark-load";
                "<enter>" = "open";
                #a = "rename";
                #r = "reload";
                #C = "clear";
                #U = "unselect";

                do = "dragon-out";

                gh = "cd";
                "g/" = "/";
                gd = "cd ~/Downloads";
                gc = "cd ~/.dotfiles";
                gt = "cd /tmp";
                gv = "cd ~/Videos";
                go = "cd ~/Documents";
                gp = "cd ~/Pictures";
                gw = "cd ~/Pictures/wallpaper";

                # go to impermanence dir
                #gH = "cd /persist/users/${config.home.homeDirectory}";

                ee = "editor-open";
                "e." = "edit-dir";
                #V = ''''$${pkgs.bat}/bin/bat --paging=always --theme=gruvbox "$f"'';

                #"<C-d>" = "5j";
                #"<C-u>" = "5k";
            };
        settings = {
            tabstop = 4;
            icons = true;
            preview = true;
            hidden = false;
            drawbox = true;
            ignorecase = true;
            relativenumber = true;
        };

        extraConfig = let
            previewer = pkgs.writeShellScriptBin "pv.sh" ''
            file=$1
            w=$2
            h=$3
            x=$4
            y=$5

            if [[ "$( ${pkgs.file}/bin/file -Lb --mime-type "$file")" =~ ^image ]]; then
                ${pkgs.kitty}/bin/kitty +kitten icat --silent --stdin no --transfer-mode file --place "''${w}x''${h}@''${x}x''${y}" "$file" < /dev/null > /dev/tty
                exit 1
            fi

            ${pkgs.pistol}/bin/pistol "$file"
              '';
              cleaner = pkgs.writeShellScriptBin "clean.sh" ''
                ${pkgs.ctpv}/bin/ctpvclear
                ${pkgs.kitty}/bin/kitty +kitten icat --clear --stdin no --silent --transfer-mode file < /dev/null > /dev/tty
              '';
            in ''
              # set cleaner ''${pkgs.ctpv}/bin/ctpvclear
          set cleaner ${cleaner}/bin/clean.sh
          set previewer ${pkgs.ctpv}/bin/ctpv
          cmd stripspace %stripspace "$f"
          setlocal ~/Projects sortby time
          setlocal ~/Projects/* sortby time
          setlocal ~/Downloads/ sortby time
        '';
        };

    };

}
