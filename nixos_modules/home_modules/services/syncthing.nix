{ config, pkgs, lib, ... }:
{
    options = {
        syncthing.enable = lib.mkEnableOption "enables syncthing";
    };

    config = lib.mkIf config.syncthing.enable {
        services.syncthing = {
            enable = true;
            extraOptions = [
                "--data-dir=/home/erik/Documents"
                "--home=/home/erik/.config/syncthing"
            ];
        };
    };
}
