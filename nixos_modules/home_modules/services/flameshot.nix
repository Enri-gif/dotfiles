{ config, pkgs, lib, ... }:
{
  options = {
    flameshot.enable = lib.mkEnableOption "enables flameshot";
  };
  
  config = lib.mkIf config.flameshot.enable {
    services.flameshot = {
      enable = true;
    };
  };
}
