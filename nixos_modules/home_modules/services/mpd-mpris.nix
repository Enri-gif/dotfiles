{ config, pkgs, lib, ... }:
{
  options = {
    mpd-mpris.enable = lib.mkEnableOption "enables mpd-mpris";
  };
  
  config = lib.mkIf config.mpd-mpris.enable {
    services.mpd-mpris = {
      enable = true;
    };
  };
}
