{ config, pkgs, lib, ... }:
{
  options = {
    playerctld.enable = lib.mkEnableOption "enables playerctld";
  };
  
  config = lib.mkIf config.playerctld.enable {
    services.playerctld = {
      enable = true;
    };
  };
}
