{ pkgs, lib, config, ... }:
{
    imports = [
        #apps
        ./apps/lf/lf.nix
        ./apps/waybar/waybar.nix
        ./apps/bat.nix
        ./apps/btop.nix
        ./apps/cava.nix
        ./apps/eza.nix
        ./apps/fastfetch.nix
        ./apps/git.nix
        ./apps/hyprlock.nix
        ./apps/lazygit.nix
        ./apps/mpv.nix
        ./apps/nixvim.nix
        ./apps/qutebrowser.nix
        ./apps/tofi.nix
        ./apps/wlogout/wlogout.nix
        ./apps/fzf.nix
        ./apps/hyprland.nix
        ./apps/firefox.nix

        #services
        ./services/dunst.nix
        ./services/mpd-mpris.nix
        ./services/playerctld.nix
        ./services/syncthing.nix

        #shell
        ./shell/zsh.nix
        ./shell/starship.nix
        
        #terminal
        ./apps/kitty.nix
        
        #fonts
        # ./fonts/fonts.nix

    ];

    #apps
    lf.enable = lib.mkDefault true;
    waybar.enable = lib.mkDefault true;
    bat.enable = lib.mkDefault true;
    cava.enable = lib.mkDefault true;
    eza.enable = lib.mkDefault true;
    fastfetch.enable = lib.mkDefault true;
    git.enable = lib.mkDefault true;
    hyprlock.enable = lib.mkDefault true;
    lazygit.enable = lib.mkDefault true;
    mpv.enable = lib.mkDefault true;
    nixvim.enable = lib.mkDefault true;
    qutebrowser.enable = lib.mkDefault true;
    tofi.enable = lib.mkDefault true;
    wlogout.enable = lib.mkDefault true;
    btop.enable = lib.mkDefault true;
    fzf.enable = lib.mkDefault true;
    hyprland.enable = lib.mkDefault true;
    firefox.enable = lib.mkDefault true;

    #services
    dunst.enable = lib.mkDefault true;
    mpd-mpris.enable = lib.mkDefault true;
    playerctld.enable = lib.mkDefault true;
    syncthing.enable = lib.mkDefault true;


    #shell
    zsh.enable = lib.mkDefault true;
    starship.enable = lib.mkDefault true;

    #terminal
    kitty.enable = lib.mkDefault true;

    #fonts
    # fonts.enable = lib.mkDefault true;
}
