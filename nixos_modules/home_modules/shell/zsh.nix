{ config, pkgs, lib, ... }:
{
    options = {
        zsh.enable = lib.mkEnableOption "enables zsh";
    };

    config = lib.mkIf config.zsh.enable {
        programs.zsh = {
            enable = true;
            autosuggestion.enable = true;
            syntaxHighlighting.enable = true;
            enableCompletion = true;
            dotDir = ".config/zsh";
            #promptInit = "source ${pkgs.zsh-powerlevel10k}/share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
            shellAliases = {
                gc = "nix-collect-garbage";
                ls = "eza --icons=always --colour=always --group-directories-first";
                cat = "bat --color=always";
                grep = "grep --color=auto";
                fzf = "fzf --preview='bat --color=always {}'";
                wine64 = "WINEPREFIX=$HOME/.wine64 wine";
                lg = "cd $HOME/.dotfiles && lazygit";
                # rm = "echo no";
                tp = "trash-put";
                te = "trash-empty";
                tl = "trash-list";
                tr = "trash-restore";
                weather = "curl wttr.in/Odense";
                sudo = "sudo ";
                rpl = "rsync -av --ignore-existing --no-times";
                server = "ssh erik@192.168.87.84"; 
                mkdir = "mkdir -p";
                open = "xdg-open";
            };
            plugins = [
                # Vi keybindings
                {
                    name = "zsh-vi-mode";
                    file = "./share/zsh-vi-mode/zsh-vi-mode.plugin.zsh";
                    src = pkgs.zsh-vi-mode;
                }
            ];

            history = {
                expireDuplicatesFirst = true;
                ignoreDups = true;
                ignoreSpace = true;
                extended = true;
                path = "${config.xdg.dataHome}/zsh/history";
                share = false;
                size = 100;
                save = 100;
            };

            #zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'

            profileExtra = ''
              setopt incappendhistory
              setopt histfindnodups
              setopt histreduceblanks
              setopt histverify
              setopt correct
              setopt correct_all
              setopt extendedglob
              setopt nocaseglob
              setopt rcexpandparam
              setopt numericglobsort
              setopt appendhistory
              setopt interactivecomments
              unsetopt nobeep
              unsetopt CASE_GLOB
              unsetopt histignorealldups
              autoload -Uz compinit && compinit
              zstyle ':completion:*' matcher-list 'r:[[:ascii:]]||[[:ascii:]]=** r:|=* m:{a-z\-}={A-Z\_}'
              zstyle ':completion:*' list-colors "''${(s.:.)LS_COLORS}"
              zstyle ':completion:*' rehash true
              zstyle ':completion:*' accept-exact '*(N)'
              zstyle ':completion:*' use-cache on
              mkdir -p "$(dirname ${config.xdg.cacheHome}/zsh/completion-cache)"
              zstyle ':completion:*' cache-path "${config.xdg.cacheHome}/zsh/completion-cache"
              zstyle ':completion:*' menu select
              WORDCHARS=''${WORDCHARS//\/[&.;]}
            '';

            #source ~/.p10k.zsh
            # powerlevel10k and fzf integration
            initExtra = ''
            source <(fzf --zsh)
            '';
        };

        programs.direnv = {
            enable = true;
            enableZshIntegration = true;
            nix-direnv.enable = true;
        };
    };

}
