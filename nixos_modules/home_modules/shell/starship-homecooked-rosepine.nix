{ config, lib, ... }:
let
    package = "starship";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption {
            default = true;
            description = "enables starship";
        };
    };

    config = lib.mkIf config.${package}.enable {
        programs.${package} = {
            enable = true;
            enableTransience = true;
            enableZshIntegration = true;
            settings = {
                format = lib.concatStrings [
                   "$os"
                   "$directory"
                   "$git_branch"
                   "$git_status"
                   "$git_metrics"
                   "$custom"
                   "$rust"
                   "$fill"
                   "$cmd_duration"
                   "$line_break"
                   "$character"

                ];

                os = {
                    disabled = false;
                    format = "[╭─  ](fg:#c4a7e7)";
                };
                character = {
                    error_symbol = " [×](bold red)";
                    success_symbol = "[╰─❯](fg:#c4a7e7)";
                };

                directory = {
                    read_only = "";
                    # style = "fg:#6e6a86"; close to the colour of terminal bg
                    # style = "fg:#b48ead";
                    # style = "fg:#ff79c6"; bright purple
                    # style = "fg:#a1a6c8"; A soft, light periwinkle with a cool tone
                    style = "fg:#8892b0";
                    format = " [$path]($style)";
                    truncation_length = 3;
                    substitutions = {
                        Documents = "󰈙 ";
                        Downloads = " ";
                        Music = " ";
                        Pictures = " ";
                    };
                };
                cmd_duration = {
                    disabled = false;
                    # style = "(fg:#f4d58d)";
                    format = "took(fg:#e0def4) [$duration](fg:#9ccfd8)";#($style)";
                };

                git_branch = {
                    symbol = "";
                    # style = "fg:#769ff0";
                    style = "fg:#2b5c73";
                    format = " [$symbol$branch]($style)";
                };

                git_status = {
                    # style = "bg:#394260";
                    format = "( [+$added ]($added_style fg:#7da88e))([-$deleted ]($deleted_style fg:#524f67))";
                    conflicted = "🏳";
                    up_to_date = "";
                    untracked = " ";
                    # ahead = "⇡${count}";
                    # diverged = "⇕⇡${ahead_count}⇣${behind_count}";
                    # behind = "⇣${count}";
                    stashed = " ";
                    modified = " ";
                    staged = "[++\($count\)](green)";
                    renamed = "襁 ";
                    deleted = " ";
                };
                git_metrics = {
                    disabled = false;
                    added_style = "fg:#7da88e";
                    deleted_style =" fg:#8c5e7b";
                    format = "( [+$added ]($added_style))([-$deleted ]($deleted_style))";
                };
                nodejs = {
                    symbol = "";
                    style = "bg:#212736";
                    format = "[[ $symbol ($version) ](fg:#769ff0 bg:#212736)]($style)";
                };

                rust = {
                    symbol = "";
                    style = "bg:#212736";
                    format = "[[ $symbol ($version) ](fg:#769ff0 bg:#212736)]($style)";
                };

                golang = {
                    symbol = "";
                    style = "bg:#212736";
                    format = "[[ $symbol ($version) ](fg:#769ff0 bg:#212736)]($style)";
                };

                php = {
                    symbol = "";
                    style = "bg:#212736";
                    format = "[[ $symbol ($version) ](fg:#769ff0 bg:#212736)]($style)";
                };
                custom.nix = {
                    detect_extensions = ["nix"];
                    symbol = "";
                    style = "bg:#212736";
                    format = "[[ $symbol ($version) ](fg:#769ff0)]($style)";
                };
                python = {
                    symbol = "";
                    style = "bg:#212736";
                    format = "[[ $symbol ($version) ](fg:#769ff0 bg:#212736)]($style)";
                };
            };
        };
    };
}

