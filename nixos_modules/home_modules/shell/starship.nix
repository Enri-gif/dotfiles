{ config, lib, ... }:
let
    package = "starship";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption {
            default = true;
            description = "enables starship";
        };
    };

    config = lib.mkIf config.${package}.enable {
        programs.${package} = {
            enable = true;
            enableTransience = true;
            enableZshIntegration = true;
            settings = {
                format = lib.concatStrings [
                   "$os"
                   "$directory"
                   "$git_branch"
                   "$git_status"
                   "$git_metrics"
                   "$custom"
                   "$rust"
                   "$fill"
                   "$cmd_duration"
                   "$line_break"
                   "$character"

                ];
                palette = "rose-pine";

                palettes.rose-pine = {
                    overlay = "#26233a";
                    love = "#eb6f92";
                    gold = "#f6c177";
                    rose = "#ebbcba";
                    pine = "#31748f";
                    foam = "#9ccfd8";
                    iris = "#c4a7e7";
                    casal = "#2b5c73";
                    # deleted = "#ea9a97";
                };

                os = {
                    disabled = false;
                    format = "[╭─  ](fg:iris)";
                };
                character = {
                    error_symbol = "[╰─](fg:iris)[×](fg:love)";
                    success_symbol = "[╰─❯](fg:iris)";
                    vicmd_symbol = "[╰─❮](fg:iris)";
                    # vicmd_symbol = "[╰─❮](fg:#8F2996)";
                };

                directory = {
                    read_only = "";
                    # style = "fg:#6e6a86"; close to the colour of terminal bg
                    # style = "fg:#b48ead";
                    # style = "fg:#ff79c6"; bright purple
                    # style = "fg:#a1a6c8"; A soft, light periwinkle with a cool tone
                    style = "fg:casal";
                    format = " [$path]($style)";
                    truncation_symbol = "../";
                    truncation_length = 3;
                    substitutions = {
                        Documents = "󰈙 ";
                        Downloads = " ";
                        Music = "󰝚";
                        Pictures = " ";
                    };
                };
                cmd_duration = {
                    disabled = false;
                    # style = "(fg:#f4d58d)";
                    # format = "took(fg:#e0def4) [$duration](fg:#9ccfd8)";
                    format = "took(fg:#e0def4) [$duration](fg:rose)";
                };

                git_branch = {
                    symbol = "";
                    # style = "fg:#769ff0";
                    # style = "fg:#2b5c73";
                    style = "fg:#foam";
                    format = "[ $symbol$branch ](fg:#9ccfd8)";
                };

                git_status = {
                    # style = "bg:#394260";
                    format = "( [+$added ]($added_style fg:#7da88e))([-$deleted ]($deleted_style fg:love))";
                    conflicted = "󰞇";
                    up_to_date = "";
                    untracked = " ";
                    # ahead = "⇡${count}";
                    # diverged = "⇕⇡${ahead_count}⇣${behind_count}";
                    # behind = "⇣${count}";
                    stashed = "󰪶";
                    modified = " ";
                    staged = "[++\($count\)](green)";
                    renamed = "󱇨";
                    deleted = " ";
                };
                git_metrics = {
                    disabled = false;
                    added_style = "fg:#7da88e";
                    deleted_style =" fg:love";
                    format = "( [+$added ]($added_style))([-$deleted ]($deleted_style))";
                };
                nodejs = {
                    symbol = "";
                    style = "fg:pine";
                    format = "[ $symbol ($version)]($style)";
                };

                rust = {
                    symbol = "";
                    style = "fg:pine";
                    format = "[ $symbol ($version)]($style)";
                };

                golang = {
                    symbol = "";
                    style = "fg:pine";
                    format = "[ $symbol ($version)]($style)";
                };

                php = {
                    symbol = "";
                    style = "fg:pine";
                    format = "[ $symbol ($version)]($style)";
                };
                custom.nix = {
                    detect_extensions = ["nix"];
                    symbol = "";
                    style = "fg:pine";
                    format = "[ $symbol ($version)]($style)";
                };
                python = {
                    symbol = "";
                    style = "fg:pine";
                    format = "[ $symbol ($version)]($style)";
                };
            };
        };
    };
}

