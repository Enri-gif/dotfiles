{ pkgs, lib, config, ... }:
{
    imports = [
        ./test_app/fastfetch.nix
	./test_app/sioyek.nix
    ];
    fastfetch.enable = lib.mkDefault true;
    sioyek.enable = lib.mkDefault true;
}
