{ config, pkgs, lib, ... }: 
  
let
  types = lib.types;
in
{
  options = {
    sioyek.enable = lib.mkEnableOption {
      types = types.bool;
      default = true;
      description = "enables sioyek";
    };
  };
  
  config = lib.mkIf config.sioyek.enable {
    programs.sioyek = {
	enable = true;
    };
  };
}

