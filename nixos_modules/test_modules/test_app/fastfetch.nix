{ config, pkgs, lib, ... }: 
  
let
  types = lib.types;
in
{
  options = {
    fastfetch.enable = lib.mkEnableOption {
      types = types.bool;
      default = true;
      description = "enables fastfetch";
    };
  };
  
  config = lib.mkIf config.fastfetch.enable {
    programs.fastfetch = {
	enable = true;
    };
  };
}

