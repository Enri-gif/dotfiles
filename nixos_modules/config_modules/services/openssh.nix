{ config, lib, ... }:
let
    package = "openssh";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };
    config = lib.mkIf config.${package}.enable {
        services.${package}.enable = true;
    };
}

