{ config, pkgs, lib, ... }:
{
    options = {
        wallpaper_timer.enable = lib.mkEnableOption "enables wallpaper_timer";
    };
    config = lib.mkIf config.wallpaper_timer.enable {
        systemd.timers."hello-world" = {
  	    wantedBy = [ "timers.target" ];
    	    timerConfig = {
      		OnBootSec = "5m";
      		OnUnitActiveSec = "5m";
      		Unit = "hello-world.service";
    	    };
	};

	systemd.services."hello-world" = {
  	    script = ''
    	        set -eu
    	        ${pkgs.coreutils}/bin/echo "Hello World"
  	    '';
  	    serviceConfig = {
    		Type = "oneshot";
    		User = "root";
  	    };
	};
    };
}

