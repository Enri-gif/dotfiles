{ config, lib, ... }:
let
    package = "xserver";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };
    config = lib.mkIf config.${package}.enable {
        services.${package} = {
            enable = true;
            xkb.layout = "dk";
            xkb.variant = "";
            videoDrivers = [ "nvidia" ];  
        };
    };
}

