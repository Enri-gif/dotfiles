{ config, lib, ... }:
let
    package = "pipewire";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };
    config = lib.mkIf config.${package}.enable {
        services.${package} = {
            enable = true;
            alsa.enable = true;
            alsa.support32Bit = true;
            pulse.enable = true;
        };
    };
}

