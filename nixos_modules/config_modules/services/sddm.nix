{ config, lib, pkgs, ... }:
let
    package = "sddm";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };
    config = lib.mkIf config.${package}.enable {
        services.displayManager.${package} = {
            enable = true;
            theme = "sddm-astronaut-theme";
            # wayland.enable = true;
            package = pkgs.kdePackages.sddm;
            extraPackages = [ pkgs.sddm-astronaut ];
        };
        environment.systemPackages = [pkgs.sddm-astronaut];
    };
}

