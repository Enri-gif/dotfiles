{ config, lib, ... }:
let
    package = "flatpak";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };
    config = lib.mkIf config.${package}.enable {
        services.${package}.enable = true;
    };
}

