{ config, pkgs, userSettings, lib, ...}:
let
    package = "rtkit";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };
    config = lib.mkIf config.${package}.enable {
        security.${package}.enable = true;
    };
}
