{ config, pkgs, userSettings, lib, ...}:
{
  options = {
    sudo.enable = lib.mkEnableOption "enables sudo";
  };
  config = lib.mkIf config.sudo.enable {
    security.sudo.enable = true;
  };
}
