{ config, lib, ... }:
let
    package = "firewall";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };
    config = lib.mkIf config.${package}.enable {
        networking.${package} = {
            enable = true;
            allowedTCPPorts = [ 8384 22000 ];
            allowedUDPPorts = [ 22000 21027 ];
        };
    };
}

