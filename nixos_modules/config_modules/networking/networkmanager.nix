{ config, lib, ... }:
let
    package = "networkmanager";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };
    config = lib.mkIf config.${package}.enable {
        networking.${package}.enable = true;
    };
}

