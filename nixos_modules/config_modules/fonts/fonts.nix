{ config, pkgs, lib, ... }:
{
    fonts.packages = with pkgs; [
        nerd-fonts.symbols-only
        font-awesome
        powerline-fonts
        powerline-symbols

        #Japanse, Chinese nad Korean fonts
        noto-fonts-cjk-sans
        noto-fonts-cjk-serif
        # source-han-serif
        # source-han-mono
        # source-han-sans
        # babelstone-han
        # sarasa-gothic
    ];
}

