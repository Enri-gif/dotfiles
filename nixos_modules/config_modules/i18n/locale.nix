{ config, lib, ... }:
{
    i18n.defaultLocale = "en_DK.UTF-8";

    i18n.extraLocaleSettings = {
        LANGUAGE = "en_UK.UTF-8";
        LC_ALL = "da_DK.UTF-8";
        LC_COLLATE = "da_DK.UTF-8";
        LC_CTYPE= "da_DK.UTF-8";
        LC_ADDRESS = "da_DK.UTF-8";
        LC_IDENTIFICATION = "da_DK.UTF-8";
        LC_MEASUREMENT = "da_DK.UTF-8";
        LC_MESSAGES = "da_DK.UTF-8";
        LC_MONETARY = "da_DK.UTF-8";
        LC_NAME = "da_DK.UTF-8";
        LC_NUMERIC = "da_DK.UTF-8";
        LC_PAPER = "da_DK.UTF-8";
        LC_TELEPHONE = "da_DK.UTF-8";
        LC_TIME = "da_DK.UTF-8";
    };
    i18n.supportedLocales = [
        "all"
    ];
}

