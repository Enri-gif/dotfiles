{ config, lib, pkgs, ... }:
let
    package = "portal";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };
    config = lib.mkIf config.${package}.enable {
        xdg.${package} = {
            enable = true;
            config.common.default = "*";
            extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
        };
    };
}

