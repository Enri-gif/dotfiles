{ config, pkgs, lib, ... }:
let
    package = "graphics";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };
    config = lib.mkIf config.${package}.enable {
        hardware.${package} = {
            enable = true;
        };
    };
}

