{ config, lib, ... }:
let
    package = "nvidia";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };
    config = lib.mkIf config.${package}.enable {
        hardware.nvidia = {
            package = config.boot.kernelPackages.nvidiaPackages.latest;
            modesetting.enable = true;
            open = false;
        };
    };
}

