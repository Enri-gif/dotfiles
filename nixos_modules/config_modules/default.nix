{ pkgs, lib, config, ... }:
{
    imports = [
        #apps
        ./apps/atool.nix
        ./apps/p7zip.nix
        ./apps/playerctl.nix
        ./apps/rar.nix
        ./apps/unrar.nix
        ./apps/unzip.nix
        ./apps/wl-clipboard.nix
        ./apps/gamemode.nix
        ./apps/bc.nix
        ./apps/wine.nix
        ./apps/libreoffice.nix
        ./apps/trash-cli.nix
        ./apps/vimiv.nix
        ./apps/git-credential-keepassxc.nix
        ./apps/git.nix
        ./apps/gnupg.nix
        ./apps/hyprshot.nix
        ./apps/nh.nix
        ./apps/keepassxc.nix
        ./apps/libnotify.nix
        ./apps/swww.nix
        ./apps/networkmanagerapplet.nix
        ./apps/pavucontrol.nix
        ./apps/osu-lazer-bin.nix
        ./apps/input-remapper.nix
        ./apps/wireshark.nix
        ./apps/vulkan-utils.nix
        ./apps/qdirstat.nix
        ./apps/libwebp.nix

        #services
        ./services/blueman.nix
        ./services/sddm.nix
        ./services/xserver.nix
        ./services/printing.nix
        ./services/flatpak.nix
        ./services/openssh.nix

        #security
        ./security/rtkit.nix

        #hardware
        ./hardware/nvidia.nix
        ./hardware/graphics.nix
        ./hardware/bluetooth.nix
        ./hardware/pulseaudio.nix

        #networking
        ./networking/networkmanager.nix
        ./networking/firewall.nix

        #boot
        ./boot/boot.nix

        #locale
        ./i18n/locale.nix

        #xdg
        ./xdg/portal.nix

        #fonts
        ./fonts/fonts.nix
    ];

    #apps
    atool.enable = lib.mkDefault true;
    p7zip.enable = lib.mkDefault true;
    playerctl.enable = lib.mkDefault true;
    rar.enable = lib.mkDefault true;
    unrar.enable = lib.mkDefault true;
    unzip.enable = lib.mkDefault true;
    wl-clipboard.enable = lib.mkDefault true;
    gamemode.enable = lib.mkDefault true;
    bc.enable = lib.mkDefault true;
    wine.enable = lib.mkDefault true;
    libreoffice.enable = lib.mkDefault true;
    trash-cli.enable = lib.mkDefault true;
    vimiv.enable = lib.mkDefault true;
    git-credential-keepassxc.enable = lib.mkDefault true;
    git.enable = lib.mkDefault true;
    gnupg.enable = lib.mkDefault true;
    hyprshot.enable = lib.mkDefault true;
    nh.enable = lib.mkDefault true;
    keepassxc.enable = lib.mkDefault true;
    libnotify.enable = lib.mkDefault true;
    swww.enable = lib.mkDefault true;
    networkmanagerapplet.enable = lib.mkDefault true;
    pavucontrol.enable = lib.mkDefault true;
    osu-lazer-bin.enable = lib.mkDefault true;
    input-remapper.enable = lib.mkDefault true;
    wireshark.enable = lib.mkDefault true;
    vulkan-utils.enable = lib.mkDefault true;
    qdirstat.enable = lib.mkDefault true;
    libwebp.enable = lib.mkDefault true;

    #stylix.enable = lib.mkDefault true;

    #services
    blueman.enable = lib.mkDefault true;
    sddm.enable = lib.mkDefault true;
    xserver.enable = lib.mkDefault true;
    printing.enable = lib.mkDefault true;
    flatpak.enable = lib.mkDefault true;
    openssh.enable = lib.mkDefault true;
    
    #security
    rtkit.enable = lib.mkDefault true;

    #hardware
    nvidia.enable = lib.mkDefault true;
    graphics.enable = lib.mkDefault true;
    bluetooth.enable = lib.mkDefault true;
    pulseaudio.enable = lib.mkDefault true;

    #networking
    networkmanager.enable = lib.mkDefault true;
    firewall.enable = lib.mkDefault true;

    #boot
    boot.enable = lib.mkDefault true;

    #xdg
    portal.enable = lib.mkDefault true;

}
