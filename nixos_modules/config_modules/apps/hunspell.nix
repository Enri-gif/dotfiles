{ config, pkgs, lib, ... }:
let
    package = "hunspell";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };
    config = lib.mkIf config.${package}.enable {
        environment.systemPackages = [
            pkgs.${package}
            pkgs.hunspellDicts.da_DA
            pkgs.hunspellDicts.uk_UA
        ];
    };
}
