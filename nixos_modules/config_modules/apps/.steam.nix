{ config, pkgs, lib, ... }:
let
    package = "steam";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };
    config = lib.mkIf config.${package}.enable {
        programs.${package} = {
            enable = true;
            protontricks.enable = true;
        };
    };
}

