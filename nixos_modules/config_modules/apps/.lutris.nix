{ config, pkgs, lib, ... }:
let
    package = "lutris";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };
    config = lib.mkIf config.${package}.enable {
        environment.systemPackages = [
            pkgs.${package}
        ];
    };
}
