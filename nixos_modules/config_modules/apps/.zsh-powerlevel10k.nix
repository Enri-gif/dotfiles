{ config, pkgs, lib, ... }:
let
    package = "zsh-powerlevel10k";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };
    config = lib.mkIf config.${package}.enable {
        environment.systemPackages = [
            pkgs.${package}
        ];
    };
}

