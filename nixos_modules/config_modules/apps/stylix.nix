{ config, pkgs, lib, ... }:
let
    package = "stylix";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };
    config = lib.mkIf config.${package}.enable {
        ${package}.image = /home/erik/Pictures/wallpaper/img/any_minute_now.png;
    };
}
