{ config, pkgs, lib, ... }:
let
    package = "wine";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };
    config = lib.mkIf config.${package}.enable {
        environment.systemPackages = [
            # 32 and 64 bit
            pkgs.wineWowPackages.stable
        ];
    };
}
