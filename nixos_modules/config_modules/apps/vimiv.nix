{ config, pkgs, lib, ... }:
let
    package = "vimiv";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };
    config = lib.mkIf config.${package}.enable {
        environment.systemPackages = [
            pkgs.vimiv-qt
        ];
    };
}

