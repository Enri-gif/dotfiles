{ config, pkgs, lib, ... }:
let
    package = "boot";
in
{
    options = {
        ${package}.enable = lib.mkEnableOption "enables ${package}";
    };
    config = lib.mkIf config.${package}.enable {
        # Bootloader.
        boot = {
            loader = {
                systemd-boot.enable = true;
                efi.canTouchEfiVariables = true;
            };

            kernelPackages = pkgs.linuxPackages_6_11;

            initrd.luks.devices."luks-83a58ef0-e783-46ad-a567-f15900d88939".device = "/dev/disk/by-uuid/83a58ef0-e783-46ad-a567-f15900d88939";
        };
    };
}

