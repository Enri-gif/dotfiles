{ buildPythonPackage, fetchFromGitHub, rustc, python3Packages, rustPlatform, pkgs, ... }:
let 
    # Override maturin to use version 1.3.2
    maturin = pkgs.maturin.overrideAttrs (oldAttrs: rec {
        version = "1.3.2";
        src = fetchFromGitHub {
            owner = "PyO3";
            repo = "maturin";
            rev = "v${version}";
            # sha256 = "11nb787mzqcls2vma9mrwlrz5gc9pv077fm9dmzbz6vynjvqzl43";
            sha256 = "sha256-sb1PSp+hS8IDyA6O4AS37xdkYBRGoX+sc0Oo8p1Si9g=";
        };
        cargoHash = "";
    });
in
buildPythonPackage rec {
    pname = "python-adblock";
    format = "pyproject";
    version = "0.6.0";

    src = fetchFromGitHub {
        inherit pname version;
        owner = "ArniDagur";
        repo = "python-adblock";
        rev = "0.6.0";
        sha256 = "0994zqzq1s1sbqi7h4dlw6p78nvg8az7s5gfqbai9zf79isp23p6";
    };

    doCheck = false;

    nativeBuildInputs = [
        rustc
        maturin
        rustPlatform.cargoSetupHook
    ];
    cargoDeps = rustPlatform.fetchCargoTarball {
        inherit pname version src;
        hash = "sha256-XF+BojY7uzvfgakM0Lwqka6FqC10yf2rhHZugCUZZLg=";
    };
    propagatedBuildInputs = [ python3Packages.setuptools ];
}
