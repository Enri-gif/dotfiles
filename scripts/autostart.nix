{ pkgs, ...}:

pkgs.writeShellScriptBin "autostart" ''
    swww-daemon &

    random_wallpaper &

    nm-applet --indicator &

    # waybar &

    dunst
''
