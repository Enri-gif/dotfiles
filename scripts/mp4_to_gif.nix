{ pkgs }:

pkgs.writeShellScriptBin "mp4_to_gif" ''
# Function to display usage information
    show_usage() {
        echo "Usage: $0 input.mp4 output.gif [duration] [width] [height]"
        echo "  input.mp4   - The input video file"
        echo "  output.gif  - The output GIF file"
        echo "  duration    - (Optional) Duration in seconds to convert or 'd' for full video (default is full video)"
        echo "  width       - (Optional) Width of the output GIF (default is original width)"
        echo "  height      - (Optional) Height of the output GIF (default is original height)"
    }

# Check if at least two arguments are provided
    if [ "$#" -lt 2 ]; then
        show_usage
        exit 1
    fi

    input_video="$1"
    output_gif="$2"
    ffmpeg=${pkgs.ffmpeg}/bin/ffmpeg
    ffprobe=${pkgs.ffmpeg}/bin/ffprobe
    gifsicle=${pkgs.gifsicle}/bin/gifsicle
    notify=${pkgs.libnotify}/bin/notify-send


# Check and assign duration
    if [ -n "$3" ]; then
        duration="$3"
    else
        duration="d"  # Default duration is 'd' meaning full video
    fi

# Check and assign width
    if [ -n "$4" ]; then
        width="$4"
    else
        width=""  # Default width is empty, meaning original width
    fi

# Check and assign height
    if [ -n "$5" ]; then
        height="$5"
    else
        height=""  # Default height is empty, meaning original height
    fi

# Get the frame rate of the input video
    frame_rate=$($ffprobe -v 0 -of csv=p=0 -select_streams v:0 -show_entries stream=r_frame_rate "$input_video" | bc -l)

# Construct filter options for ffmpeg
    filters="fps=$frame_rate"

# Append scale filter if width and height are provided
    if [ -n "$width" ] && [ -n "$height" ]; then
        filters="$filters,scale=$width:$height:flags=lanczos"
    elif [ -n "$width" ]; then
        filters="$filters,scale=$width:-1:flags=lanczos"
    elif [ -n "$height" ]; then
        filters="$filters,scale=-1:$height:flags=lanczos"
    fi

# Add palettegen to the filters with max_colors set to 256
    filters="$filters,palettegen=max_colors=256"

# Generate a palette
    if [ "$duration" != "d" ]; then
        $ffmpeg -t "$duration" -i "$input_video" -vf "$filters" -y palette.png
    else
        $ffmpeg -i "$input_video" -vf "$filters" -y palette.png
    fi

# Construct filter options for paletteuse
    filters="fps=$frame_rate"

# Append scale filter if width and height are provided
    if [ -n "$width" ] && [ -n "$height" ]; then
        filters="$filters,scale=$width:$height:flags=lanczos"
    elif [ -n "$width" ]; then
        filters="$filters,scale=$width:-1:flags=lanczos"
    elif [ -n "$height" ]; then
        filters="$filters,scale=-1:$height:flags=lanczos"
    fi

# Add paletteuse to the filters
    filters="$filters[x];[x][1:v]paletteuse"

# Use the palette to create the GIF
    if [ "$duration" != "d" ]; then
        $ffmpeg -t "$duration" -i "$input_video" -i palette.png -filter_complex "$filters" -y "$output_gif"
    else
        $ffmpeg -i "$input_video" -i palette.png -filter_complex "$filters" -y "$output_gif"
    fi

# Optimize the GIF (optional)
    $gifsicle -O3 "$output_gif" -o "$output_gif"

# Remove the palette file
    rm palette.png

    echo "GIF created successfully: $output_gif"
    $notify "done converting $input_video to $output_gif"
''
