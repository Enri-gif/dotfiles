{ pkgs, ...}:
{
    imports = [
        ./autostart.nix
        ./mp4_to_gif.nix
        ./random_wallpaper.nix
    ];
}
