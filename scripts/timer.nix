{ pkgs, ...}:

pkgs.writeShellScriptBin "timer" ''
    clear
    sleep $1
    ${pkgs.libnotify}/bin/notify-send "done"
''
