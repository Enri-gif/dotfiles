{ pkgs, ...}:

pkgs.writeShellScriptBin "window_switcher" ''
    ${pkgs.hyprland}/bin/hyprctl clients -j | ${pkgs.jq}/bin/jq -r '.[] | .class + " " + .title + " " + .address' | ${pkgs.tofi}/bin/tofi | ${pkgs.coreutils}/bin/cut -d' ' -f3 | ${pkgs.findutils}/bin/xargs hyprctl dispatch focuswindow address
''
