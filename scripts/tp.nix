{ pkgs, ...}:

pkgs.writeShellScriptBin "tp" ''
    for arg in "$@"; do
        ${pkgs.trash-cli}/bin/trash-put $arg
    done
''
