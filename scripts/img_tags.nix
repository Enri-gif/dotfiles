{ pkgs, ...}:

pkgs.writeShellScriptBin "img_tags" ''
# Define custom namespace
    NAMESPACE="H_tags"
    NS_URI="http://ns.example.com/H_tags"

# Path to exiftool provided by Nix
    EXIFTOOL="${pkgs.exiftool}/bin/exiftool -config ./.ExifTool_config"

# Ensure namespace exists for non-GIF images
    ensure_namespace() {
        file="$1"
        if ! echo "$file" | grep -qE '\.gif$'; then
            # Define the custom namespace and add an empty tag set if necessary
            $EXIFTOOL -overwrite_original \
                -XMP-"$NAMESPACE"="$NS_URI" \
                -XMP-"$NAMESPACE":Tags="" "$file" >/dev/null
        fi
    }

# Add tags to images
    add_tags() {
        image="$1"
        shift
        tags="$@"

        ensure_namespace "$image"

        if echo "$image" | grep -qE '\.gif$'; then
            # Handle GIFs using a sidecar file
            sidecar="$image.tags"
            [ -f "$sidecar" ] && existing_tags=$(cat "$sidecar") || existing_tags=""
            for tag in $tags; do
                if ! echo "$existing_tags" | grep -qx "$tag"; then
                    echo "$tag" >>"$sidecar"
                    echo "Added tag '$tag' to $sidecar"
                else
                    echo "Tag '$tag' already exists in $sidecar"
                fi
            done
        else
            # Handle other image types using exiftool
            for tag in $tags; do
                existing_tags=$($EXIFTOOL -XMP-"$NAMESPACE" "$image" 2>/dev/null | awk -F': ' '{print $2}' | tr ',' '\n' | sed 's/^\s*//;s/\s*$//')
                if ! echo "$existing_tags" | grep -qx "$tag"; then
                    $EXIFTOOL -overwrite_original -XMP-"$NAMESPACE"+="$tag" "$image" >/dev/null
                    echo "Added tag '$tag' to $image"
                else
                    echo "Tag '$tag' already exists in $image"
                fi
            done
        fi
    }

# Find images with specified tags
    get_images_with_tags() {
        tags="$@"
        find . -type f \( -iname "*.jpg" -o -iname "*.png" -o -iname "*.jpeg" -o -iname "*.gif" \) | while read -r file; do
            if echo "$file" | grep -qE '\.gif$'; then
                # Check tags in sidecar file for GIFs
                sidecar="$file.tags"
                [ -f "$sidecar" ] && all_tags=$(cat "$sidecar") || all_tags=""
            else
                # Check tags in metadata for other image types
                all_tags=$($EXIFTOOL -XMP-"$NAMESPACE" "$file" 2>/dev/null | awk -F': ' '{print $2}' | tr ',' '\n' | sed 's/^\s*//;s/\s*$//')
            fi

            for tag in $tags; do
                if echo "$all_tags" | grep -qx "$tag"; then
                    echo "$file"
                    break
                fi
            done
        done
    }

# Edit an existing tag
    edit_tag() {
        image="$1"
        old_tag="$2"
        new_tag="$3"

        if echo "$image" | grep -qE '\.gif$'; then
            # Handle GIFs using sidecar files
            sidecar="$image.tags"
            if [ -f "$sidecar" ]; then
                sed -i "s/^$old_tag$/$new_tag/" "$sidecar" && echo "Replaced tag '$old_tag' with '$new_tag' in $sidecar"
            else
                echo "No tags found for $image"
            fi
        else
            # Handle other image types using exiftool
            existing_tags=$($EXIFTOOL -XMP-"$NAMESPACE" "$image" 2>/dev/null | awk -F': ' '{print $2}' | tr ',' '\n' | sed 's/^\s*//;s/\s*$//')
            if echo "$existing_tags" | grep -qx "$old_tag"; then
                $EXIFTOOL -overwrite_original -XMP-"$NAMESPACE"-="$old_tag" "$image" >/dev/null
                $EXIFTOOL -overwrite_original -XMP-"$NAMESPACE"+="$new_tag" "$image" >/dev/null
                echo "Replaced tag '$old_tag' with '$new_tag' in $image"
            else
                echo "Tag '$old_tag' not found in $image"
            fi
        fi
    }

# Delete tags from an image
    delete_tags() {
        image="$1"
        shift
        tags="$@"

        if echo "$image" | grep -qE '\.gif$'; then
            # Handle GIFs using sidecar files
            sidecar="$image.tags"
            if [ -f "$sidecar" ]; then
                for tag in $tags; do
                    sed -i "/^$tag$/d" "$sidecar" && echo "Deleted tag '$tag' from $sidecar"
                done
            else
                echo "No tags found for $image"
            fi
        else
            # Handle other image types using exiftool
            for tag in $tags; do
                existing_tags=$($EXIFTOOL -XMP-"$NAMESPACE" "$image" 2>/dev/null | awk -F': ' '{print $2}' | tr ',' '\n' | sed 's/^\s*//;s/\s*$//')
                if echo "$existing_tags" | grep -qx "$tag"; then
                    $EXIFTOOL -overwrite_original -XMP-"$NAMESPACE"-="$tag" "$image" >/dev/null
                    echo "Deleted tag '$tag' from $image"
                else
                    echo "Tag '$tag' not found in $image"
                fi
            done
        fi
    }

# Main script logic
    while getopts ":t:f:e:d:" opt; do
        case $opt in
            t)
                shift $((OPTIND - 1))
                images=""
                tags=""
                for arg in "$@"; do
                    if [ -f "$arg" ]; then
                        images="$images $arg"
                    else
                        tags="$tags $arg"
                    fi
                done
                for image in $images; do
                    add_tags "$image" $tags
                done
                ;;
            f)
                shift $((OPTIND - 1))
                tags="$@"
                get_images_with_tags $tags
                ;;
            e)
                shift $((OPTIND - 1))
                image="$1"
                old_tag="$2"
                new_tag="$3"
                edit_tag "$image" "$old_tag" "$new_tag"
                ;;
            d)
                shift $((OPTIND - 1))
                image="$1"
                shift
                tags="$@"
                delete_tags "$image" $tags
                ;;
            *)
                echo "Usage:"
                echo "  $0 -t <image1> <image2> ... <tag1> <tag2> ..." >&2
                echo "  $0 -f <tag1> <tag2> ..." >&2
                echo "  $0 -e <image> <old_tag> <new_tag>" >&2
                echo "  $0 -d <image> <tag1> <tag2> ..." >&2
                exit 1
                ;;
        esac
    done
''
