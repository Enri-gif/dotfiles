#!/bin/bash

# Get the list of monitors
MONITORS=$(hyprctl monitors | grep "Monitor" | awk '{print $2}')

# Get the workspaces of each monitor
WORKSPACE1=$(hyprctl workspaces | grep -B 1 "$(echo $MONITORS | awk '{print $1}')" | head -n 1 | awk '{print $2}')
WORKSPACE2=$(hyprctl workspaces | grep -B 1 "$(echo $MONITORS | awk '{print $2}')" | head -n 1 | awk '{print $2}')

# Swap the workspaces
hyprctl dispatch workspace $WORKSPACE2 monitor $(echo $MONITORS | awk '{print $1}')
hyprctl dispatch workspace $WORKSPACE1 monitor $(echo $MONITORS | awk '{print $2}')
