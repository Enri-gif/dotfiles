{ pkgs, ...}:

pkgs.writeShellScriptBin "template" ''
    base_template="$HOME/.dotfiles/templates"

    # use input to search template dir with find and copy that to target dir

    template_file=find "$base_template" -name $1

    cp template_file .
''
