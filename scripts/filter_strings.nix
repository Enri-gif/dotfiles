{ pkgs, ...}:

pkgs.writeShellScriptBin "filter_strings" ''
# Check if the file is provided as an argument
    if [ $# -ne 1 ]; then
        echo "Usage: $0 <strings_output_file>"
        exit 1
    fi

# Input file (strings output)
    strings_file="$1"

# Define the keywords to search for (these are common indicators for various languages)
    keywords="msvcrt vcruntime cl.exe GCC MinGW kernel32.dll user32.dll gdi32.dll System mscorlib java python"

# Loop through the keywords and filter lines containing them
    for keyword in $keywords; do
        echo "Searching for '$keyword'..."

        # Use grep -c to count the number of lines matching the keyword, ignoring case
        count=$(grep -ic "$keyword" "$strings_file")
        
        # Print the count if there are any matches
        if [ "$count" -gt 0 ]; then
            echo "$keyword: $count occurrences."
        fi
        
        echo "---------------------------------"
    done

    echo "Finished searching through the strings."
''
