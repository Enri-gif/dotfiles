{ pkgs, ...}:

pkgs.writeShellScriptBin "random_wallpaper" ''

    # Use find and shuf to select a random file without storing all file paths in memory
    random_file=$(find "$HOME/Pictures/wallpaper/" -type f \( -name "*.jpg" -o -name "*.png" -o -name "*.gif" \) | shuf -n 1)

    # Check if a file was found
    if [ -z "$random_file" ]; then
      echo "No files found in the directories."
      exit 1
    fi

    # Set the wallpaper with swww
    swww img "$random_file"
''
