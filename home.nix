{ pkgs, lib, ... }:
{
    imports = [
        ./nixos_modules/home_modules/default.nix
    ];
    nixpkgs = {
        config = {
            allowUnfree = true;
            allowUnfreePredicate = (_: true);
    	};
    };
    # Home Manager needs a bit of information about you and the paths it should
    # manage.

    home.username = "erik";
    home.homeDirectory = "/home/erik";

    # This value determines the Home Manager release that your configuration is
    # compatible with. This helps avoid breakage when a new Home Manager release
    #introduces backwards incompatible changes.
    # You should not change this value, even if you update Home Manager. If you do
    # want to update the value, then make sure to first check the Home Manager
    # release notes.
    home.stateVersion = "24.05"; # Please read the comment before changing.

    # Enable flakes
    nix = {
        # I get the error that nix.package is already defined
        #package = pkgs.nix;
        settings.experimental-features = [ "nix-command" "flakes" ];
    };

    # Modules
    nixvim.enable = lib.mkForce true;

    stylix.enable = true;
    stylix.image = ./101405981_p1.jpg;
    stylix.targets.kitty.enable = true;
    stylix.base16Scheme = "${pkgs.base16-schemes}/share/themes/rose-pine.yaml";
    # https://github.com/edunfelt/base16-rose-pine-scheme

    # The home.packages option allows you to install Nix packages into your
    # environment.
    home.packages = with pkgs; [
    ];

    fonts.fontconfig.enable = true;

    # home-manager manages dotfiles
    home.file = {
        ".config/vimiv/vimiv.conf".source = ./config/vimiv.conf;
    };

    home.sessionVariables = {};

    # Let Home Manager install and manage itself.
    programs.home-manager.enable = true;
}
