{ fetchFromGitHub, rustPlatform }:
rustPlatform.buildRustPackage rec {
    pname = "dtkit-patch";
    version = "0.1.6";
    
    src = fetchFromGitHub {
        owner = "manshanko";
        repo = pname;
        rev = version;
        hash = "sha256-pGTS0Jk6ZxJj36cjQty/fLKDi67SVPBOp/wyylIfWZ0=";
    };

    cargoLock = {
        lockFile = ./Cargo.lock;
        outputHashes = {
             "steam_find-0.1.2" = "sha256-lH03qTPNvKbGNB2+RfI3BTgbOJ+T/R7q2kGcv8zoflg=";
        };
    };

    meta = {
        description = "dtkit-patch for darktide modloader";
        homepage = "https://github.com/manshanko/dtkit-patch";
        liscense = "";
        maintainers = [];
    };
}
