# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, inputs, ... }:
{
    imports =
    [ # Include the results of the hardware scan.
        ./hardware-configuration.nix
        inputs.home-manager.nixosModules.default

        # Modules
        ./nixos_modules/config_modules/default.nix

        # inputs.hyprland.nixosModules.default

    ];


    networking.hostName = "nixos"; # Define your hostname.
    # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

    # Configure network proxy if necessary
    # networking.proxy.default = "http://user:password@proxy:port/";
    # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domai

    #Modules
    pulseaudio.enable = false;

    stylix.enable = true;
    stylix.image = ./101405981_p1.jpg;
    stylix.base16Scheme = "${pkgs.base16-schemes}/share/themes/rose-pine.yaml";

    programs.hyprland = {
        enable = true;
        package = inputs.hyprland.packages."${pkgs.system}".hyprland;
    };

    environment.sessionVariables = {
        #If your cursor becomes invisible
        #WLR_NO_HARDWARE_CURSORS = "1";	  
        
        #Hint electron apps to use wayland
        NIXOS_OZONE_WL = "1";

        # dotfiles location 
        FLAKE = "$HOME/.dotfiles";
    };

    #nix.settings.experimental-features = [ "nix-command" "flakes" ];

    # Set your time zone.
    time.timeZone = "Europe/Copenhagen";

    # Configure console keymap
    console.keyMap = "dk-latin1";

    # Define a user account. Don't forget to set a password with ‘passwd’.
    users.users.erik = {
        isNormalUser = true;
        description = "erik";
        shell = pkgs.zsh;
        extraGroups = [ "networkmanager" "wheel" "gamemode" ];
    };
    
    home-manager = {
        extraSpecialArgs = { inherit inputs; };
        users = {
            "erik" = import ./home.nix;
        };
    };
    # enable authentication with hyprlock
    security.pam.services.hyprlock = {};

    # Allow unfree packages
    nixpkgs = {
        config.allowUnfree = true;
        overlays = [
            (import ./overlays/dtkit-patch/dtkit-patch.nix)
        ];
    };

    # fonts.packages = with pkgs; [
    #     nerd-fonts.symbols-only
    #     font-awesome
    #     powerline-fonts
    #     powerline-symbols
    # ];


    # List packages installed in system profile. To search, run:
    environment.systemPackages = with pkgs; [
        dtkit-patch

        #fonts

        # font-awesome
        # powerline-fonts
        # powerline-symbols
        # nerd-fonts.symbols-only

        # scripts
        #(import ./scripts/default.nix { inherit pkgs; }) doesn't work. this returns a set but environment.systemPackages needs binaries
        (import ./scripts/autostart.nix { inherit pkgs; })
        (import ./scripts/mp4_to_gif.nix { inherit pkgs; })
        (import ./scripts/random_wallpaper.nix { inherit pkgs; })
        (import ./scripts/timer.nix { inherit pkgs; })
        (import ./scripts/window_switcher.nix { inherit pkgs; })
        (import ./scripts/img_tags.nix { inherit pkgs; })
        (import ./scripts/filter_strings.nix { inherit pkgs; })
        (import ./scripts/template.nix { inherit pkgs; })
        (import ./scripts/tp.nix { inherit pkgs; })

        jq

    ];

    # powerlevel10k
    # programs.zsh = {
    #     promptInit = "source ${pkgs.zsh-powerlevel10k}/share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
    # };

    programs.zsh.enable = true;
    environment.shells = with pkgs; [ zsh ];

    # programs.mtr.enable = true;
    # programs.gnupg.agent = {
    #   enable = true;
    #   enableSSHSupport = true;
    # };

    # This value determines the NixOS release from which the default
    # settings for stateful data, like file locations and database versions
    # on your system were taken. It‘s perfectly fine and recommended to leave
    # this value at the release version of the first install of this system.
    # Before changing this value read the documentation for this option
    # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
    system.stateVersion = "23.11"; # Did you read the comment?

}
